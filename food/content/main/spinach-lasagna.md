---
title: "Spinach lasagna"
date: 2019-01-14T17:35:00+02:00
image: "spinach-lasagna.jpg"
tags: [olive oil, onion, mushrooms, garlic, spinach, walnuts,
       goat cheese, lasagna sheets]
---
* 2 ts olive oil
* 1 big onion
* 250g mushrooms
<!--more-->
* pepper and salt

Bake for 5 mins in a tall pot

* 2 crushed garlic cloves
* 1 kg spinach

Lay ¼ of the mix over lasagna sheets in an oiled 1.5l bowl.
Sprinkle with ¼ of 300g grated goat cheese. Repeat. Add walnuts on top
and bake for 25 mins at 180°.
