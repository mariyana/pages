---
title: "Gnocchi with shrimps"
date: 2019-01-14T17:00:00+02:00
image: "gnocchi-shrimps.jpg"
tags: [gnocchi, shrimps, sambal, canned tomatoes, 
       mozzarella, rucola]
---
* 500g gnocchi di patate
* 200g shrimps
* 1 ts sambal oelek
<!--more-->
* 400g canned tomatoes
* 125g mozzarella

Cook in the oven for 15 mins on 150°. Serve with 75 g rucola.
