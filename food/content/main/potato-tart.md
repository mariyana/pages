---
title: "Potato tart with leaks, mushrooms and mustard"
date: 2019-03-17T12:30:00+02:00
image: "potato-tart.jpg"
tags: [flour, mustard, butter, potato, olive oil, leeks,
       mushrooms, ]
---
For the potato pastry:

* 100g plain flour, plus extra for dusting
* salt and freshly ground black pepper
<!--more-->

* 1 tsp English mustard powder
* 75g chilled unsalted butter, diced
* 100g mashed potato
* 1 tbsp iced water

Bake for 15 mins at 180ºC.

For the filling:

* 2 tbsp olive oil
* 400g leeks, trimmed and thinly sliced
* 100g chestnut mushrooms, sliced
* salt and freshly ground black pepper
* 75g mascarpone
* 1 tbsp Dijon mustard
* 50g Cornish blue, Stilton or Gorgonzola (or a similar vegetarian blue cheese), diced
* 1 crushed dried chillies (optional)

Bake for 45 mins at 180ºC.

Source [bbc.com](https://www.bbc.com/food/recipes/potato_tart_with_mustard_36933)
