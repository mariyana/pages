---
title: "Peanut burger"
date: 2018-12-30T12:09:00+02:00
image: "peanut-burger.jpg"
tags: [chickpeas, oatmeal, garlic, onion, peanuts,
       sour cream, harissa, curry, coconut milk, avocado, rucola]
---
* 200g chickpeas
* 6 ts oatmeal
* 4 garlic cloves
<!--more-->
* 1 red onion
* 200g peanuts
* 2 ts sour cream
* 1 ts harissa
* 1 ts curry powder
* salt and pepper
* 1 ts coconut milk

Blend in a food processor. Bake in a pan 3 mins per side.
Serve on bread with avocado and rucola.

