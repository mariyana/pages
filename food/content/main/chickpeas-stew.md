---
title: "Chickpeas stew with carrots"
date: 2019-01-14T16:45:00+02:00
image: "chickpeas-stew.jpg"
tags: [onion, harissa, minced meat, celery, tomatoes,
       carrots, cilantro, chickpeas, olives]
---
* 1 onion
* 2 tbsp harissa
* 500g minced meat
<!--more-->
* 3 celery stalks
* 400g canned tomatoes
* 250ml water
* 6 carrots
* 20g cilantro
* 1 ts salt en peper

Cook for 10 minutes in a pot.

* 400 g canned chickpeas
* 75g olives

Serve with couscous or rice and a dash of yogurt.
