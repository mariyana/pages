---
title: "Thai meatballs"
date: 2019-01-07T23:30:00+02:00
image: "meatballs.jpg"
tags: [minced meat, egg, breadcrumbs, jalapeno, cilantro,
       coconut milk, tomato paste, lime, onion, garlic, cheese, ginger]
---
* 500g minced beef meat
* 1 egg
<!--more-->
* ½ cup breadcrumbs
* 1 jalapeno pepper
* 1 cup cilantro
* ½ cup condensed coconut milk
* ¼ cup tomato paste
* ¼ cup lime juice
* 1 onion
* 3 cloves garlic
* 1 cup grated old cheese
* 1 inch grated ginger root
* salt and pepper

Bake on a tray with baking paper for 30 mins at 150ºC.
