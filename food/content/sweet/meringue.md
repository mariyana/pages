---
title: Meringue
date: 2019-04-29T12:00:00+02:00
image: meringue.jpg
tags: [egg, sugar, salt, aroma, ginger]
---
* 3 egg whites
* 1 cup crystal sugar
* ½ ts salt
<!--more-->
* ½ ts vanilla aroma
* 1cm grated ginger root

Bake 1½ hour at 90°.
