---
title: Panna cotta
date: 2019-04-29T13:00:00+02:00
image: panna-cotta.jpg
tags: [milk, gelatin, sugar, coconut]
---
* 1/2 cup whole milk
* 3 gelatin leaves

Simmer for 5 mins until gelatin is dissolved and add:
<!--more-->

* 1½ cup whole milk
* 1 cup coconut milk
* ½ cup white sugar
* 1 cup dessicated coconut

Take away from fire when mix starts to boil. Serve with jam and mint leaf.
