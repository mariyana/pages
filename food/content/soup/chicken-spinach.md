---
title: "Chicken spinach soup"
date: 2018-12-30T12:37:00+02:00
image: "chicken-spinach.jpg"
tags: [olive oil, celery, onion, carrots, broth, rice, chicken, spinach, dill]
---
* 1 ts olive oil
* 3 ribs of celery, each sliced lengthwise down the middle then crosswise into thin pieces 
* 1 small yellow onion, diced small or 1 leek
<!--more-->
* 500g carrots

Cook for 5 mins

* 1,5l chicken broth (1 jar 500ml magie broth + 3 jars water)
* 2 cups rice, uncooked

Simmer for 10 mins until rice is almost done. Turn off fire and add

* 500g shredded, cooked chicken
* 500g packed fresh spinach, roughly chopped
* 1/3 cup chopped fresh dill
* 1 ts salt and 1/2 ts fresh ground pepper
* 1 beaten egg
* 1 cup yogurt
* 1 ts lemon juice

Cover for 2 mins with lid on. Serve with lemon wedges and spring onion.
