---
title: "Garlic soup"
date: 2019-01-14T17:30:00+02:00
image: "garlic.jpg"
tags: [butter, onion, thyme, garlic, celery, sage, stock,
       heavy cream, lemon, parmesan]
---
* 20g butter
* 1 big onion
* 1 ts thyme
<!--more-->

Bake for 5 min

* 15 cloves garlic
* 25 cloves garlic, baked in the oven with olive oil, salt and pepper at 200° for 20 mins
* 3 celery stalks
* 6 leaves sage

Bake another 3 mins

* 4 cups vegetable stock

Simmer 10 mins until garlic cloves get soft and blend.

* 1 cup heavy cream
* salt and pepper

Simmer for another 3 mins. Serve with lemon juice and parmesan cheese.
