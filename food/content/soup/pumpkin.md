---
title: "Pumpkin soup"
date: 2019-01-14T17:15:00+02:00
image: "pumpkin.jpg"
tags: [onions, garlic, ginger, carrots, pumpkin, apple, orange,
       sage, mint, cayenne pepper, chorizo, cottage cheese]
---
* 2 onions
* 3 cloves garlic
* 1 inch ginger root
<!--more-->
Roast 3 mins in a big pot.

* 2 big carrots
* 1 small pumpkin
* 1l water
* 1 ts salt and black pepper

Boil 5 mins.

* 1 apple
* juice from 1 orange
* 5 mint leaves
* 5 sage leaves
* 1 spoon cayenne pepper

Boil 5 more mins and blend.
Serve with cottage cheese and grilled chorizo.
