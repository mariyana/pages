---
title: "Ricotta mint sauce"
date: 2018-12-30T12:29:00+02:00
image: "ricotta-mint.jpg"
tags: [cottage cheese, mint, chives]
---
* 200 g AH cottage cheese
* 10 g fresh mint (of parsley or basil)
* 5 g fresh chives
<!--more-->
optional: spring pea, lime

Blend in a food processor.
