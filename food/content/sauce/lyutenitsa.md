---
title: "Lyutenitsa"
date: 2019-04-12T14:00:00+02:00
image: "lyutenitsa.jpg"
tags: [peppers, carrot, eggplant, onion,
       olive oil, vinegar, sugar, white wine]
---
* 3kg red bell peppers
* 2kg tomato puree
* 500g carrots
<!--more-->
* 1kg eggplants
* 1 big onion
* 3 sl salt
* ¾ cup olive oil
* ¾ cup vinegar
* ¾ cup sugar
* 100g white wine

In the oven for 2 hours at 250°C. Blend and boil in jars for 30 mins.
